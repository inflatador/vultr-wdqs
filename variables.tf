
variable "num_servers" {
  type    = number
  default = 1
}


variable "dns_domain" {
  type    = string
  default = "public-cloud.net"

}

variable "flavor" {
  type    = string
  default = "vc2-4c-8gb"
}
# as of 2023-01-29: 352 is Buster; 477 is Debian Bullseye; 1929 is Fedora 37;
# 542 is CentOS 9 Stream; 1868 is Alma 9

variable "os_id" {
  type    = string
  default = "477"
}

variable "script_id" {
  type    = string
  default = ""
}

variable "srv_name" {
  type    = string
  default = "vultr-base"
}
variable "ssh_pubkeys" {
  type    = list(any)
  default = [""]
}

variable "region" {
  type    = string
  default = ""
}
variable "vultr_api_key" {
  type    = string
  default = ""
}
