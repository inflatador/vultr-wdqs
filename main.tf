
data "template_file" "vultr_user_data" {
  template = file("${path.module}/user-data.yml.tmpl")
  vars = {
    srv_name   = "${var.srv_name}"
    dns_domain = var.dns_domain
  }

}

resource "vultr_bare_metal_server" "wdqs_mirror" {
  hostname    = var.srv_name
  label       = var.srv_name
  plan        = "vbm-8c-132gb"
  region      = var.region
  os_id       = var.os_id
  enable_ipv6 = "true"
  ssh_key_ids = var.ssh_pubkeys
  user_data   = data.template_file.vultr_user_data.rendered

  lifecycle {
    ignore_changes = [ user_data ]
  }

}

# if you don't supply a priority, the vultr DNS API
# automatically sets one, then the terraform provider
# tries to delete it every time you apply

resource "vultr_dns_record" "a_record" {
  depends_on = [vultr_bare_metal_server.wdqs_mirror]
  domain     = var.dns_domain
  name       = vultr_bare_metal_server.wdqs_mirror.hostname
  data       = vultr_bare_metal_server.wdqs_mirror.main_ip
  type       = "A"
  priority   = "1"
}

resource "vultr_dns_record" "quad_a_record" {
  depends_on = [vultr_bare_metal_server.wdqs_mirror]
  domain     = var.dns_domain
  name       = vultr_bare_metal_server.wdqs_mirror.hostname
  data       = vultr_bare_metal_server.wdqs_mirror.v6_main_ip
  type       = "AAAA"
  priority   = "1"
}

resource "local_file" "ansible_inventory" {
  content = templatefile("${path.root}/ansible/hosts.tmpl", {
    srv_name   = "${var.srv_name}"
    dns_domain = var.dns_domain
    }
  )
  filename        = "ansible/hosts.ini"
  file_permission = "0644"
  # Sleep a few minutes so DNS can propagate, then run playbook. 
}
