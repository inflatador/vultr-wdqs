
output "wdqs_mirror_ip" {
  description = "wdqs_mirror_ip"
  value       = vultr_bare_metal_server.wdqs_mirror.main_ip
}

output "vultr_fqdn" {
  description = "FQDN"
  value       = "${vultr_dns_record.a_record.name}.${vultr_dns_record.a_record.domain}"
}
